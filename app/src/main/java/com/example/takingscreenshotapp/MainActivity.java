package com.example.takingscreenshotapp;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.app.Dialog;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Point;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;

public class MainActivity extends AppCompatActivity {

    Dialog dialog;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        showFullScreenDialog();
        setUpWindowListener();
    }


    public void takeSnapshot(View view) {
        dialog.dismiss();

        Activity currentActivity = this;
        Display display = currentActivity.getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int width = size.x;
        int height = size.y;

        Log.d("SIZE","width = "+width+" height = "+height);
        View mScreenShotView = view.getRootView();


        //this is deprecated from api 28
        mScreenShotView.setDrawingCacheEnabled(true);
        Log.d("SIZE","width = "+mScreenShotView.getDrawingCache().getWidth()+" height = "+mScreenShotView.getHeight());
        Bitmap bm = Bitmap.createBitmap(mScreenShotView.getDrawingCache(), 0, 0, mScreenShotView.getDrawingCache().getWidth(), mScreenShotView.getHeight());

        Bitmap bitmap = createBitmapFromView(mScreenShotView);
        setBitmapToImageView(bitmap);
    }

    public void setBitmapToImageView(Bitmap bitmap) {
        ImageView imageView = findViewById(R.id.image);
        imageView.setImageBitmap(bitmap);
    }


    Bitmap createBitmapFromView(@NonNull View view) {


        view.layout(0, 0, view.getMeasuredWidth(), view.getMeasuredHeight());
        Log.d("SIZE","width = "+view.getMeasuredWidth()+" height = "+view.getMeasuredHeight());

        Bitmap bitmap = Bitmap.createBitmap(view.getMeasuredWidth(),
                view.getMeasuredHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        Drawable background = view.getBackground();

        if (background != null) {
            background.draw(canvas);
        }
        view.draw(canvas);

        return bitmap;
    }

    public void showFullScreenDialog() {

        View view = LayoutInflater.from(this).inflate(R.layout.dialog,null);
        dialog = new Dialog(this,android.R.style.Theme_Translucent_NoTitleBar_Fullscreen);
        dialog.setContentView(view);
        dialog.show();
    }

    // to remove the bottom navigation bar
    private void setUpWindowListener() {
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.KITKAT){
            // SYSTEM_UI_FLAG_IMMERSIVE is supported from api 19
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_HIDE_NAVIGATION | View.SYSTEM_UI_FLAG_IMMERSIVE);
            View decorView = getWindow().getDecorView();
            decorView.setOnSystemUiVisibilityChangeListener
                    (new View.OnSystemUiVisibilityChangeListener() {
                        @Override
                        public void onSystemUiVisibilityChange(int visibility) {
                            // Note that system bars will only be "visible" if none of the
                            // LOW_PROFILE, HIDE_NAVIGATION, or FULLSCREEN flags are set.
                            if ((visibility & View.SYSTEM_UI_FLAG_FULLSCREEN) == 0) {
                                // TODO: The system bars are visible. Make any desired
                                // adjustments to your UI, such as showing the action bar or
                                // other navigational controls.

                            }
                        }
                    });
        }
    }

}
